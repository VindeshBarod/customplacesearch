package com.vindesh.CustomPlaceAPI;

import com.vindesh.CustomPlaceAPI.controller.SearchPlaceController;
import com.vindesh.CustomPlaceAPI.exception.SearchPlaceException;
import com.vindesh.CustomPlaceAPI.model.CustomResponseWrapper;
import com.vindesh.CustomPlaceAPI.service.SearchPlaceService;
import com.vindesh.CustomPlaceAPI.service.impl.SearchPlaceServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SearchPlaceControllerTest {

    @InjectMocks
    private SearchPlaceController searchPlaceController;

    @Mock
    private SearchPlaceServiceImpl searchPlaceService;

    @Test
    public void test_getFormattedAddress_positive() throws Exception {
        CustomResponseWrapper r = new CustomResponseWrapper();
        r.setFormattedAddress("Pune, Maharashtra, India");
        Mockito.when(searchPlaceService.getFormattedAddress(Mockito.anyString())).thenReturn(r);
        CustomResponseWrapper formattedAddress = searchPlaceController.getFormattedAddress("Pune");
        Assert.assertEquals(r.getFormattedAddress(),formattedAddress.getFormattedAddress());
    }

    @Test(expected = NullPointerException.class)
    public void test_getFormattedAddress_negative_flow() {

    }

}
