package com.vindesh.CustomPlaceAPI.model;

public class CustomResponseWrapper {

    private String formattedAddress;

    private String source;

    public CustomResponseWrapper(String source,String formattedAddress) {
        this.formattedAddress = formattedAddress;
        this.source = source;
    }



    public String getFormattedAddress() {
        return formattedAddress;
    }

    @Override
    public String toString() {
        return "CustomResponseWrapper{" +
                "formattedAddress='" + formattedAddress + '\'' +
                '}';
    }

    public String getSource() {
        return source;
    }
}
