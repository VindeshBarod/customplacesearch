package com.vindesh.CustomPlaceAPI.model;

public class InputSearchWrapper {

    private String place;

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
