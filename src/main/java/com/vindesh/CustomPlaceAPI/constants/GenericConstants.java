package com.vindesh.CustomPlaceAPI.constants;

public class GenericConstants {

    public static final String EXCEPTION_MESSAGE = "Something went wrong while searching for place";
    public static final String THIRD_PARTY_EXCEPTION_MESSAGE = "Something went wrong while hitting third party API";
}
