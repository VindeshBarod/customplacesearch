package com.vindesh.CustomPlaceAPI.service;

import com.vindesh.CustomPlaceAPI.exception.SearchPlaceException;
import com.vindesh.CustomPlaceAPI.model.CustomResponseWrapper;

import java.util.List;

public interface SearchPlaceService {

    public List<CustomResponseWrapper> getFormattedAddress(String placeToSearch) throws Exception;

}
