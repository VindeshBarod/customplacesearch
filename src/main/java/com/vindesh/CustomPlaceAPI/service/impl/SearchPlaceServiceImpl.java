package com.vindesh.CustomPlaceAPI.service.impl;

import com.vindesh.CustomPlaceAPI.constants.GenericConstants;
import com.vindesh.CustomPlaceAPI.exception.SearchPlaceException;
import com.vindesh.CustomPlaceAPI.exception.ThirdPartyApiException;
import com.vindesh.CustomPlaceAPI.factory.APIFactory;
import com.vindesh.CustomPlaceAPI.model.CustomResponseWrapper;
import com.vindesh.CustomPlaceAPI.model.InputSearchWrapper;
import com.vindesh.CustomPlaceAPI.service.SearchPlaceService;
import com.vindesh.CustomPlaceAPI.thirdPartyApis.ThirdPartyApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchPlaceServiceImpl implements SearchPlaceService {

    private Logger logger = LoggerFactory.getLogger(SearchPlaceServiceImpl.class);

    @Value("${THIRD_PARTY_API_LIST}")
    private List<String> thirdPartyApiList;

    @Autowired
    private APIFactory apiFactory;

    @Override
    public CustomResponseWrapper getFormattedAddress(String placeToSearch) throws Exception {
        logger.info("going to get formatted address for searchString : {}",placeToSearch);
        InputSearchWrapper inputSearchWrapper = new InputSearchWrapper();
        inputSearchWrapper.setPlace(placeToSearch);
        for (String name : thirdPartyApiList) {
            try {
                ThirdPartyApi thirdPartyApi = apiFactory.getInstance(name);
                return thirdPartyApi.getPlaceDetails(inputSearchWrapper);
            } catch (ThirdPartyApiException e) {
                logger.error("Error while getting formatted Address ThirdPartyApiException ",e.getMessage());
                e.printStackTrace();
            }
        }
        logger.error("Error while getting formatted Address getPlaceDetails ");
        throw  new SearchPlaceException(GenericConstants.EXCEPTION_MESSAGE);
    }
}
