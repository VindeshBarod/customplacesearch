package com.vindesh.CustomPlaceAPI.controller;

import com.vindesh.CustomPlaceAPI.constants.GenericConstants;
import com.vindesh.CustomPlaceAPI.exception.SearchPlaceException;
import com.vindesh.CustomPlaceAPI.model.CustomResponseWrapper;
import com.vindesh.CustomPlaceAPI.service.SearchPlaceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class SearchPlaceController {

    private static Logger logger = LoggerFactory.getLogger(SearchPlaceController.class);

    @Autowired
    private SearchPlaceService searchPlaceService;

    @GetMapping("/isServiceUp")
    public String isServiceUpAndRunning() {
        return "service is up and running successfully";
    }

    @RequestMapping(path = "/getFormattedAddress/{place}")
    public List<CustomResponseWrapper> getFormattedAddress(@PathVariable("place") String placeToSearch) throws SearchPlaceException {
        logger.info("going to get formatted address for searchString : {}",placeToSearch);
        try {
            List<CustomResponseWrapper> formattedAddressList = searchPlaceService.getFormattedAddress(placeToSearch);

            formattedAddressList.stream().filter(f -> "Google".equals(f.getSource())).collect(Collectors.toList());

        } catch(Exception e) {
            logger.error("Error while getting formatted Address ",e.getMessage());
            throw new SearchPlaceException(GenericConstants.EXCEPTION_MESSAGE);
        }
    }

}
