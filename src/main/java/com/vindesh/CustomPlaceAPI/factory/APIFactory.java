package com.vindesh.CustomPlaceAPI.factory;

import com.vindesh.CustomPlaceAPI.service.impl.SearchPlaceServiceImpl;
import com.vindesh.CustomPlaceAPI.thirdPartyApis.ThirdPartyApi;
import com.vindesh.CustomPlaceAPI.thirdPartyApis.impl.GooglePlaceAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class APIFactory {

    private Logger logger = LoggerFactory.getLogger(APIFactory.class);

    @Autowired
    private GooglePlaceAPI googlePlaceAPI;

    public ThirdPartyApi getInstance(String name) {
        logger.info("going to get instance for {}",name);
        if("GooglePlaceAPI".equals(name)) {
                return googlePlaceAPI;
        } else {
            logger.info("Not got instance for {}",name);
            return null;
        }
    }
}
