package com.vindesh.CustomPlaceAPI.exception;

public class SearchPlaceException extends Exception {

    private String message;

    public SearchPlaceException(String message) {
        super(message);
        this.message = message;
    }

    public String getMesssage() {
        return  message;
    }

}
