package com.vindesh.CustomPlaceAPI.exception;

import com.vindesh.CustomPlaceAPI.constants.GenericConstants;

public class ThirdPartyApiException extends Exception {


    public ThirdPartyApiException() {
        super(GenericConstants.THIRD_PARTY_EXCEPTION_MESSAGE);
    }

}
