package com.vindesh.CustomPlaceAPI.thirdPartyApis.impl;

import com.vindesh.CustomPlaceAPI.exception.ThirdPartyApiException;
import com.vindesh.CustomPlaceAPI.model.CustomResponseWrapper;
import com.vindesh.CustomPlaceAPI.model.GoogleSearchResponse;
import com.vindesh.CustomPlaceAPI.model.InputSearchWrapper;
import com.vindesh.CustomPlaceAPI.model.Result;
import com.vindesh.CustomPlaceAPI.thirdPartyApis.ThirdPartyApi;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class GooglePlaceAPI implements ThirdPartyApi {

    private Logger logger = LoggerFactory.getLogger(GooglePlaceAPI.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${google.api.url}")
    private String url;

    @Value("${google.api.key}")
    private String apiKey;

    @Override
    public CustomResponseWrapper getPlaceDetails(InputSearchWrapper inputSearchWrapper) throws ThirdPartyApiException {
        CustomResponseWrapper customResponseWrapper = new CustomResponseWrapper();
        String placeToSearch = inputSearchWrapper != null ? inputSearchWrapper.getPlace() : "";
        try {
            logger.info("going to get places from google API");
            String completeUrl = url + placeToSearch + "&key=" + apiKey;
//            ResponseEntity<GoogleSearchResponse> googleSearchResponseEntity = restTemplate.getForEntity(completeUrl, GoogleSearchResponse.class);
//            GoogleSearchResponse googleSearchResponse = googleSearchResponseEntity.getBody();
            GoogleSearchResponse googleSearchResponse = getGoogleSearchResponse();
            if(googleSearchResponse != null) {
                if(googleSearchResponse.getResults() != null) {
                    if(googleSearchResponse.getResults().get(0) != null) {
                        customResponseWrapper.setFormattedAddress(googleSearchResponse.getResults().get(0).getFormattedAddress());
                        return customResponseWrapper;
                    }
                }
            }
            logger.error("Not geeting proper response from Google API");
            throw new ThirdPartyApiException();
        }catch(Exception e) {
            logger.error("Error while getting place from Google API",e.getMessage());
            throw new ThirdPartyApiException();
        }
    }

    private GoogleSearchResponse getGoogleSearchResponse() {
        GoogleSearchResponse g = new GoogleSearchResponse();
        List<Result> l = new ArrayList();
        Result r = new Result();
        r.setFormattedAddress("Pune, Maharashta, India");
        l.add(r);
        g.setResults(l);
        return g;
    }
}
