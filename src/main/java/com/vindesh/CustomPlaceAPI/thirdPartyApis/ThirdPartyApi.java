package com.vindesh.CustomPlaceAPI.thirdPartyApis;

import com.vindesh.CustomPlaceAPI.exception.ThirdPartyApiException;
import com.vindesh.CustomPlaceAPI.model.CustomResponseWrapper;
import com.vindesh.CustomPlaceAPI.model.InputSearchWrapper;

public interface ThirdPartyApi {

    public CustomResponseWrapper getPlaceDetails(InputSearchWrapper inputSearchWrapper) throws ThirdPartyApiException;
}
